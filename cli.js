function getHelp(){
	console.log("#########################################");
    console.log("#\tSmart Contract Generator CLI\t#");
    console.log("#\t\tby Felix Franz\t\t#");
    console.log("#\thttps://www.felix-franz.com\t#");
    console.log("#########################################");
    console.log("Commandline Arguments:");
    console.log("-c\t--headless\tRun without desktop.");
    console.log("-p\t--port\t\tSpecify the port on which the server is running.");
    process.exit(0);
}

module.exports.getConfigFromCommandline = function(){
    let parameters = process.argv;
    parameters.shift();
    parameters.shift();
    let config = require(__dirname + "/config");
    for (let i=0; i<parameters.length; ++i){
        if (parameters[i] === "--help" || parameters[i] === "-h"){
            getHelp();
        } else if(parameters[i] === "--headless" || "-c" === parameters[i] ){
            if (parameters[i+1] !== undefined && parameters[i+1].slice(0, 1) !== "-"){
                config.headless = 'true' === parameters[++i];
            } else
                config.headless = true;
        } else if(parameters[i] === "--port" || "-p" === parameters[i]){
            let port = parseInt(parameters[i+1]);
            if (typeof port !== "number" && 0 > port && 65535 < port || parameters[i+1] === undefined){
                console.log("Port must be a number between 0 and 65535!");
                process.exit(1);
            }
            config.port = port;
            ++i;
        }
    }
    return config;
};
