# Smart Contract Generator

The smart contract generator uses a template based approach to build Smart Contracts for [Ethereum](https://ethereum.org).

This project is [available on Gitlab](https://gitlab.com/FelixFranz/FHWS/smart-contract-generation/generator)!

## Gitlab Repository Installation

### Local execution

Use start scripts to start the generator.

1. Install [node.js](https://nodejs.org/)
1. Clone Repository `git clone git@gitlab.com:FelixFranz/FHWS/smart-contract-generation/generator.git`
1. Open Folder `cd generator`
1. Run `Start.bat` on Windows or `Start.sh` on Linux to start the generator

#### Commandline arguments

Using the start scripts also allowes you to use commandline arguments.

Checkout available commands using `./Start.sh --help` or `Start.bat --help`

E.g. start generator in hedless mode using one of following commands:
* Windows: `Start.bat --headless`
* Linux: `./Start.sh --headless`

### Docker container execution

1. Install [Docker](https://www.docker.com/)
1. Start Image on port 8080: `sudo docker run -p 8080:8080 -d --name generator registry.gitlab.com/felixfranz/fhws/smart-contract-generation/generator:latest`

## NPM Installation

Run NPM package from [NPM Registry](https://www.npmjs.com/package/smart-contract-generator)

1. Install [node.js](https://nodejs.org/)
1. Install Smart Contract Generator: `npm install -g smart-contract-generator`
1. Start generator: `smart-contract-generator`

## Docker Installation

Run Docker image using [Docker Hub](https://hub.docker.com/r/felixfranz/smart-contract-generator)

You need to run following command:
```bash
sudo docker run -p 8080:8080 -d --name generator felixfranz/smart-contract-generator
```