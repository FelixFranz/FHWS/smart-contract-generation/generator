FROM node
LABEL maintainer=dev@felix-franz.com
# port of the running webapp:
EXPOSE 8080

# create app folder
RUN mkdir /app

# create new user
RUN useradd -ms /bin/bash generator

# add generator to docker image
COPY ./ /app/

# change app folder ownership
RUN chown -R generator /app

#use new user
USER generator

# create database, database user and insert demo data
RUN cd /app/ && npm install

WORKDIR /app
CMD cd /app && npm run start-headless
