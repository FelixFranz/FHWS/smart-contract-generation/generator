#!/usr/bin/env node

let config = require(__dirname + "/cli").getConfigFromCommandline();
let apiAddress = 'http://127.0.0.1:3001';


//start backend
require(__dirname + "/backend/index");


//start proxy
let express = require('express');
let app = express();
let proxy = require('express-http-proxy');
app.use('/api', proxy(apiAddress));
app.use('/', express.static('frontend'));
app.get('/*', (req, res) => {
	res.sendFile(__dirname + '/frontend/index.html');
});
app.listen(config.port);


setTimeout(() => {
    console.log("Started generator on http://127.0.0.1:" + config.port);
    if (!config.headless)
    	require(__dirname + '/node_modules/nw/bin/nw')
}, 1000);